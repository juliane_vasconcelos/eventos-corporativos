package br.com.galgo.eventos;

import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.galgo.testes.recursos_comuns.enumerador.Canal;
import br.com.galgo.testes.recursos_comuns.enumerador.Grupo;
import br.com.galgo.testes.recursos_comuns.enumerador.Operacao;
import br.com.galgo.testes.recursos_comuns.enumerador.Servico;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.enumerador.config.MassaDados;
import br.com.galgo.testes.recursos_comuns.enumerador.menu.SubMenu;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Categoria;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.Papel;
import br.com.galgo.testes.recursos_comuns.enumerador.usuario.UsuarioConfig;
import br.com.galgo.testes.recursos_comuns.exception.ErroAplicacao;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtils;
import br.com.galgo.testes.recursos_comuns.file.ArquivoUtilsFundos;
import br.com.galgo.testes.recursos_comuns.file.entidades.EventoTrocaAdmin;
import br.com.galgo.testes.recursos_comuns.file.entidades.Teste;
import br.com.galgo.testes.recursos_comuns.file.entidades.Usuario;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaGalgo;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaHome;
import br.com.galgo.testes.recursos_comuns.pageObject.TelaLogin;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaCarteiras;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaEntidade;
import br.com.galgo.testes.recursos_comuns.pageObject.consulta.TelaConsultaFundo;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaAlteracaoAdmin;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaConferencia;
import br.com.galgo.testes.recursos_comuns.pageObject.fundo.TelaEventosCorporativos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesFundos;
import br.com.galgo.testes.recursos_comuns.utils.ConstantesTestes;
import br.com.galgo.testes.recursos_comuns.utils.TesteUtils;

public class AlteracaoAdministrador {

	Teste teste;
	Operacao operacao;

	@Before
	public void setUp() throws Exception {
		Ambiente ambiente = Ambiente.HOMOLOGACAO;
		List<Teste> listaTeste = ArquivoUtils.getListaTeste(ambiente,
				ConstantesTestes.CAMINHO_TESTE_FUNDOS,//
				ConstantesFundos.getAbaTeste(ambiente),//
				ConstantesFundos.LINHA_INICIAL_TESTE_FUNDOS,//
				ConstantesFundos.COLUNA_GRUPO_FUNDOS,//
				ConstantesFundos.COLUNA_SERVICO_FUNDOS,//
				ConstantesFundos.COLUNA_CANAL_FUNDOS,//
				ConstantesFundos.COLUNA_ID_FUNDOS,//
				ConstantesFundos.COLUNA_OPERACAO_FUNDOS,//
				ConstantesFundos.COLUNA_DESCRICAO_FUNDOS,//
				ConstantesFundos.COLUNA_RETESTE_FUNDOS);
		Grupo grupo = Grupo.DEPOIS;
		Servico servico = null;
		operacao = Operacao.ALTERACAO_ADMIN;
		Canal canal = Canal.PORTAL;

		teste = Teste.fromListaTeste(listaTeste, ambiente, grupo, servico,
				operacao, canal);
	}

	@Test
	public void alterar() throws ErroAplicacao {
		alterar(teste);
	}

	@After
	public void tearDown() {
		TesteUtils.finalizarTeste("alteracaoAdmin.png");
	}

	public void alterar(Teste teste) {
		Ambiente ambiente = teste.getAmbiente();
		final String caminhoMassaDadosFundos = MassaDados.fromAmbiente(
				ambiente, ConstantesTestes.DESC_MASSA_DADOS_FUNDOS).getPath();

		String login = ArquivoUtils.getLogin(teste, caminhoMassaDadosFundos);
		String senha = ArquivoUtils.getSenha(teste, caminhoMassaDadosFundos);
		Usuario usuario = new Usuario(login, senha, ambiente);

		String aba = ArquivoUtils.getAba(teste, caminhoMassaDadosFundos);

		List<EventoTrocaAdmin> listaEventos = ArquivoUtilsFundos
				.getEventoTrocaAdmin(teste, caminhoMassaDadosFundos, aba,
						operacao);

		final String url = ambiente.getUrl();
		TelaGalgo.abrirBrowser(url);

		for (EventoTrocaAdmin evento : listaEventos) {
			alterar(usuario, evento);
			TelaGalgo.reiniciarBrowser(url);
			confirmar(ambiente, evento);
			validarAlteracao(ambiente, evento, usuario);
		}
	}

	private void validarAlteracao(Ambiente ambiente, EventoTrocaAdmin evento,
			Usuario usuario) {
		TelaHome telaHome = new TelaHome(usuario);
		TelaConsultaCarteiras telaConsultaCarteiras = (TelaConsultaCarteiras) telaHome
				.acessarSubMenu(SubMenu.CONSULTA_CARTEIRAS_ADMINISTRADAS);

		telaConsultaCarteiras.buscarFundo(evento.getCnpj());
		telaConsultaCarteiras.clicarConfirmar();
		Assert.assertTrue("Não foi realizada a troca de Admin.",
				telaConsultaCarteiras.verificaTextoNaTela(evento
						.getAdministrador().getDesc()));

	}

	private void alterar(Usuario usuario, EventoTrocaAdmin evento) {
		TelaLogin telaLogin = new TelaLogin();
		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaEventosCorporativos telaEventosCorporativos = (TelaEventosCorporativos) telaHome
				.acessarSubMenu(SubMenu.EVENTOS_CORPORATIVOS);

		TelaAlteracaoAdmin telaAlteracaoAdmin = telaEventosCorporativos
				.clicarBotaoAlterarAdmin();
		TelaConsultaFundo telaConsultaFundo = telaAlteracaoAdmin
				.clicarBotaoIncluirFundo();
		telaConsultaFundo.incluirFiltroCNPJ(evento.getCnpj());

		TelaConsultaEntidade telaConsultaEntidade = telaAlteracaoAdmin
				.clicarBotaoIncluirAdmin();
		telaConsultaEntidade.filtrarPorNomeFantasia(evento.getAdministrador()
				.getDesc());

		telaAlteracaoAdmin.preencheDataEvento(evento.getDataEvento());

		telaEventosCorporativos.clicarBotaoConfirmar();
	}

	private void confirmar(Ambiente ambiente, EventoTrocaAdmin evento) {
		TelaLogin telaLogin = new TelaLogin();

		Usuario usuario = new Usuario(UsuarioConfig.fromCategoria(ambiente,
				Categoria.USUARIO_FINAL, Papel.CONSUMIDOR));

		TelaHome telaHome = telaLogin.loginAs(usuario);
		TelaEventosCorporativos telaEventosCorporativos = (TelaEventosCorporativos) telaHome
				.acessarSubMenu(SubMenu.EVENTOS_CORPORATIVOS);

		TelaConferencia telaConferencia = telaEventosCorporativos
				.clicarBotaoConferencia();

		telaConferencia.verificaTextoNaTela(evento.getCnpj());
		telaConferencia.clicarItem();
		telaConferencia.clicarConfirmar();
		telaConferencia.clicarAceitar();
	}
}
